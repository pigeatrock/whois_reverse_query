const puppeteer = require('puppeteer');
const fs = require('fs');
const dns = require('dns');
const dnsPromises = dns.promises;
const mysql = require('mysql');
var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'WDELI1995',
	database: 'sss'
});
connection.connect();

const searcher = require('node-ip2region').create();

(async () => {
	const browser = await puppeteer.launch({
		headless: false
	});
	
	var page = await browser.newPage();
	await page.setDefaultNavigationTimeout(120000)
	await page.goto(
		'http://whois.chinaz.com'
	);
	
	await page.waitFor(30000);
	await page.close();

	var isHas = true //是否还有数据没抓取
	var totalPage = undefined; //总页数
	var pageNum = 1 //当前的页面

	function insertData(domain, ip, address) {
		return new Promise((resolve, reject) => {
			var addSql = 'INSERT INTO urls(url,ip,city,region) VALUES(?,?,?,?)';
			var addSqlParams = [domain, ip, address.city, address.region];
			connection.query(addSql, addSqlParams, function(err, result) {
				console.log('ip: %s city: %s region: %s', ip, address.city, address.region);
				resolve(result)
				if (err) {
					console.log('[INSERT ERROR] - ', err.message);
					reject();
				}
				// console.log('--------------------------INSERT----------------------------');
				//console.log('INSERT ID:',result.insertId);        
				// console.log('INSERT ID:',result);        
				// console.log('-----------------------------------------------------------------\n\n');  
			});
		})
	}

	async function newPage(num = 1) {
		console.log('当前第:' + num + '页')
		const page = await browser.newPage();
		await page.setDefaultNavigationTimeout(60000)
		await page.goto(
			'http://whois.chinaz.com/reverse?host=**5655270@163.com&domain=hcdiwcf.cn&ddlSearchMode=1&st=&startDay=&endDay=&wTimefilter=$wTimefilter&page=' +
			num
		);

		if (!totalPage) {
			var total = await page.$('#pagelist span.col-gray02')
			let ds = await total.evaluateHandle(a => a.innerText, total)
			totalPage = Number((await ds.jsonValue()).replace(/[^0-9]/ig, ""))
		}

		var lilist = await page.$$('.WhoListCent.Wholist .domain .listOther a')
		var liLength = lilist.length
		for (let i = 0; i < liLength; i++) {
			try {
				let fd = await lilist[i].evaluateHandle(a => a.text, lilist[i])
				var domain = await fd.jsonValue()
				domain1 = 'www.' + domain
				var ipRes = await dnsPromises.lookup(domain1)
				var ip = ipRes.address
				var address = searcher.btreeSearchSync(ip)

				await insertData(domain, ip, address)
			} catch (e) {
				console.log('e: ', e)
			}
		}


		// fs.writeFile('./data/urls.txt', arr, (error) => {
		// 	if (error) {
		// 		console.log('写入失败')
		// 	} else {
		// 		console.log('写入成功了')
		// 	}
		// })
		// console.log(arr)
		await page.close();
	}

	while (isHas) {
		if (pageNum >= totalPage) {
			isHas = false
		}
		await newPage(pageNum++)
	}
	connection.end();
})();
